package com.academiadecodigo.librarymanagementsystem;

public class Main {
    public static void main(String[] args) {

        Book book = new Book("1984", "Orson Wells");
        book.displayInfo();
    }
}
