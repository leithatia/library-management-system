package com.academiadecodigo.librarymanagementsystem;

public class Book {
    // PROPERTIES
    private String title;
    private String author;
    private boolean isAvailable; // False if book is checked out

    // CONSTRUCTORS
    public Book(String title, String author) {
        this.title = title;
        this.author = author;
        isAvailable = true;
    }

    public Book(String title, String author, Boolean isAvailable) {
        this.title = title;
        this.author = author;
        this.isAvailable = isAvailable;
    }

    // METHODS
    public void displayInfo() {
        String availability = isAvailable ? "available" : "not available";
        System.out.printf("'%s' by %s is %s.", title, author, availability);
    }
}
